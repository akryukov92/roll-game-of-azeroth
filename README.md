# Роллбой для World of Warcraft #
Набор упрощенных правил для организации отыгрыша с боевкой

* Актуальная версия: 0.001 alpha
* [Дизайн роллбоя](https://valkyrie-wow.org/forum/index.php?showtopic=20087#entry220837)
* Используемые технологии: Spring, Websockets, PostgreSQL 9.5

### Инструкция по запуску ###
* Скомпилить
* Задеплоить в томкат

### Контакты ###
Рекомендуемый способ связи: Skype или email

* Админ репозитория Alexander Kryukov (email: xudojnikomsk@gmail.com, Skype: akryukov55)
* Дизайнер правил Egor Latyshev (email: ignimortis@gmail.com, Skype:ignimortis)