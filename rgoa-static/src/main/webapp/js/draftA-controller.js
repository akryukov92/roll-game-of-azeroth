var dataFactory = function($http){
    return {
        categories: function() {
            return $http.get("/game/creation/categories");
        },
        races: function(){
            return $http.get("/game/creation/races");
        },
        attributes: function() {
            return $http.get("/game/creation/attributes");
        },
        submitDraftA: function(draftA){
            return $http.post("/game/creation/submitDraftA", draftA);
        },
        possibilities: function(){
            return {
                mage:{
                    points:20, featureSlots:2, limitationSlots:2,
                    limitations:[
                        { id:"forget4circle", description:"Отказ от 4 круга", cost:10, maxCount:1},
                        { id:"forget34circle", description:"Отказ от 3 и 4 круга", cost:30, maxCount:1},
                        { id:"forget2schools", description:"", cost:10, maxCount:1}
                    ],
                    features:[
                        { id:"dmg1add", description:"+1 урона", cost:20, maxCount:2, value:0 },
                        { id:"spellsaveroll", description:"+1 к спасброскам заклинаний или стихии", cost:2, maxCount:20 },
                        { id:"special", description:"Связанная с избранной темой способность, не работающая в бою", cost:10, maxCount:2 },
                        { id:"spellhaste", description:"Ускорение чародейства", cost:30, maxCount:1 }
                    ]
                },
                default:{
                    points:30, featureSlots:3, limitationSlots:1,
                    limitations:[],
                    features:[]
                }
            }
        },
        upgradeCost: function(){
            return {
                indexDiff: -4,
                costs: [1,1,1,1,1,1,2,3]
        //     }
        // },
        // weaponProficiencies: function () {
        //     return {
        //         axes: { name: "Топоры", learned:false },
        //         swords: { name: "Мечи", learned:false },
        //         maces: { name: "Булавы", learned:false },
        //         daggers: { name: "Кинжалы", learned:false },
        //         crossbows: { name: "Арбалеты", learned:false },
        //         guns: { name: "Ружья", learned:false },
        //         light_thrown: { name: "Легкое метательное", learned:false },
        //         heavy_thrown: { name: "Тяжелое метательное", learned:false },
        //         staves: { name: "Посохи", learned:false },
        //         polearms: { name: "Древковое", learned:false },
        //         unarmed: { name: "Безоружный бой", learned:false },
        //         shields: { name: "Щиты", learned:false },
        //         exotic1: { name: "Экзотическое оружие", learned:false },
        //         exotic2: { name: "Экзотическое оружие", learned:false },
        //         exotic3: { name: "Экзотическое оружие", learned:false },
        //         exotic4: { name: "Экзотическое оружие", learned:false }
            };
        }
    }
};
angular.module('game',[])
.value('version', 'v1.0.1')
.factory('dataFactory', dataFactory)
.controller('draftAController', function draftAController($scope, dataFactory) {
    $scope.categories = [];
    $scope.races = [];
    $scope.attributes = {};
    $scope.upgradeCost = dataFactory.upgradeCost();
    $scope.creation = {
        race:{},
        category:{},
        specAttr:"fake",
        powerSource:"nil",
        attributes:{},
        mage:true,
        spec:{}
        //, weaponProficiencies:dictsFactory.weaponProficiencies()
    };
    $scope.getPowerSources = function(){
        var ret = {};
        for (var attr in $scope.attributes){
            if ($scope.attributes[attr].type == "magical"){
                ret[attr] = $scope.attributes[attr];
            }
        }
        return ret;
    };
    $scope.getPhysicalAttributes = function(){
        var ret = {};
        for (var attr in $scope.attributes){
            if ($scope.attributes[attr].type == "physical"){
                ret[attr] = $scope.attributes[attr];
            }
        }
        return ret;
    };
    $scope.chooseRace = function(){
        $scope.creation.category = {};
        $scope.creation.attributes = {};
        for (var attr in $scope.attributes){
            $scope.creation.attributes[attr] ={
                raceMin:-4,
                raceMax:4,
                value:-1,
                name:$scope.attributes[attr].name
            };
        }
        for (var j = 0; j < $scope.creation.race.limits.length; j++){
            var key = $scope.creation.race.limits[j].attributeName;
            $scope.creation.attributes[key].raceMin = $scope.creation.race.limits[j].min;
            $scope.creation.attributes[key].value = $scope.creation.attributes[key].raceMin;
            $scope.creation.attributes[key].raceMax = $scope.creation.race.limits[j].max;
        }
    };
    $scope.defineAttributesBounds = function(){
        for (var attr in $scope.creation.attributes){
            if ($scope.attributes[attr].type == "magical"){
                $scope.creation.attributes[attr].categoryMax = 4;
            } else if ($scope.attributes[attr].type == "physical"){
                if (attr == $scope.creation.specAttr){
                    $scope.creation.attributes[attr].categoryMax = $scope.creation.category.highestSpecAttribute;
                } else {
                    $scope.creation.attributes[attr].categoryMax = $scope.creation.category.highestBaseAttribute;
                }
            }
            var j = $scope.creation.attributes[attr].raceMin;
            $scope.creation.attributes[attr].values = [];
            while (j <= $scope.creation.attributes[attr].raceMax && j <= $scope.creation.attributes[attr].categoryMax){
                $scope.creation.attributes[attr].values.push(j);
                j++;
            }
        }
    };
    $scope.calculateFreeSP = function () {
        var spentSP = 0;
        for (var attr in $scope.creation.attributes){
            for (var j = 0; j < $scope.creation.attributes[attr].value - $scope.upgradeCost.indexDiff; j++){
                spentSP += $scope.upgradeCost.costs[j];
            }
        }
        return $scope.creation.category.baseSP - spentSP;
    };
    $scope.calculateHP = function(){
        if (typeof $scope.creation.attributes["end"] == 'undefined'){
            return 0;
        }
        var fromAttr = $scope.creation.attributes["end"].value * 2;
        var fromRace = $scope.creation.race.bonusHP;
        var fromCategory = $scope.creation.category.baseHP;
        return fromCategory + fromAttr + fromRace;
    };
    $scope.calculateMP = function(){
        if (!$scope.creation.mage){
            return 0;
        }
        if (typeof $scope.creation.powerSource == 'undefined'){
            return 0;
        }
        if (typeof $scope.creation.attributes[$scope.creation.powerSource] == 'undefined'){
            return 0;
        }
        var power = $scope.creation.attributes[$scope.creation.powerSource].value;
        if (power <= 0){
            return 0;
        }
        var fromAttr = power * power * 2;
        var baseMP = 10;
        var fromRace = $scope.creation.race.bonusMP;
        return (baseMP + fromAttr + fromRace);
    };
    $scope.calculateEP = function(){
        if (typeof $scope.creation.attributes["end"] == 'undefined'){
            return 0;
        }
        var fromAttr = $scope.creation.attributes["end"].value * 2;
        var fromCategory = $scope.creation.category.baseHP;
        var fromRaceEP = $scope.creation.race.bonusEP;
        return fromAttr + fromRaceEP + fromCategory;
    };
    $scope.toggleMage = function(){
        if ($scope.creation.mage){
            $scope.creation.spec = dataFactory.possibilities().mage;
        } else {
            $scope.creation.spec = dataFactory.possibilities().default;
        }
        for (var i = 0; i < $scope.creation.spec.features.length; i++){
            $scope.creation.spec.features[i].value = 0;
        }
        for (var i = 0; i < $scope.creation.spec.limitations.length; i++){
            $scope.creation.spec.limitations[i].value = 0;
        }
    };
    var getByIdFromArray = function(array, id){
        for (var i = 0; i < array.length; i++){
            if (array[i].id == id){
                return array[i];
            }
        }
        return null;
    };
    $scope.canAddFeature = function(f){
        var item = getByIdFromArray($scope.creation.spec.features, f.id);
        return item.value < item.maxCount;
    };
    $scope.canSubFeature = function(f){
        var item = getByIdFromArray($scope.creation.spec.features, f.id);
        return item.value > 0;
    };
    $scope.canAddLimitation = function(f){
        var item = getByIdFromArray($scope.creation.spec.features, f.id);
        return item.value < item.maxCount;
    };
    $scope.canSubLimitation = function(f){
        var item = getByIdFromArray($scope.creation.spec.features, f.id);
        return item.value > 0;
    };
    $scope.addToFeature = function (f) {
        var item = getByIdFromArray($scope.creation.spec.features, f.id);
        if (item.value < item.maxCount) {
            item.value++;
        }
    };
    $scope.subFromFeature = function (f) {
        var item = getByIdFromArray($scope.creation.spec.features, f.id);
        if (item.value > 0){
            item.value --;
        }
    };
    $scope.addToLimitation = function(l){
        var item = getByIdFromArray($scope.creation.spec.limitations, l.id);
        if (item.value < item.maxCount){
            item.value++;
        }
    };
    $scope.subFromLimitation = function(l){
        var item = getByIdFromArray($scope.creation.spec.limitations, l.id);
        if (item.value > 0){
            item.value --;
        }
    };

    $scope.getSpecInfo = function(){
        var featuresCount = 0;
        var featuresPoints = 0;
        for (var i = 0; i < $scope.creation.spec.features.length; i++){
            var feature = $scope.creation.spec.features[i];
            if (feature.value > 0){
                featuresCount++;
                featuresPoints += feature.value * feature.cost;
            }
            if (feature.value > feature.maxCount){
                return false;
            }
        }
        var limitsCount = 0;
        var limitsPoints = 0;
        for (var i = 0; i < $scope.creation.spec.limitations.length; i++){
            var limit = $scope.creation.spec.limitations[i];
            if (limit.value > 0){
                limitsCount++;
                limitsPoints += limit.value * limit.cost;
            }
            if (limit.value > limit.maxCount){
                return false;
            }
        }
        return {
            spentPoints:featuresPoints,
            maxPoints:($scope.creation.spec.points + limitsPoints),
            featureCount:featuresCount,
            limitsCount:limitsCount
        }
    };
    $scope.submit = function(){
        var specInfo = $scope.getSpecInfo();
        if (specInfo.spentPoints > specInfo.maxPoints ||
            specInfo.featureCount > $scope.creation.spec.featureSlots ||
            specInfo.limitsCount > $scope.creation.spec.limitationSlots
        ){
            return;
        }
        var data = {
            raceId: $scope.creation.race.id,
            categoryId: $scope.creation.category.id,
            mage: $scope.creation.mage,
            powerSource: $scope.creation.powerSource,
            specAttr: $scope.creation.specAttr,
            attributes: {},
            features: {},
            limitations:{}
        };
        for(var i = 0; i < $scope.creation.spec.features.length; i++){
            if ($scope.creation.spec.features[i].value > 0) {
                data.features[$scope.creation.spec.features[i].id] = $scope.creation.spec.features[i].value;
            }
        }
        for(var i = 0; i < $scope.creation.spec.limitations.length; i++){
            if ($scope.creation.spec.limitations[i].value > 0) {
                data.limitations[$scope.creation.spec.limitations[i].id] = $scope.creation.spec.limitations[i].value;
            }
        }
        for (var attr in $scope.creation.attributes){
            data.attributes[attr] = $scope.creation.attributes[attr].value;
        }
        dataFactory.submitDraftA(data).success(function(data){
            $scope.creation.draftA = data;
        });
    };
    dataFactory.attributes().success(function(data){
        $scope.attributes = data;
    });
    dataFactory.categories().success(function(data){
        $scope.categories = data;
    });
    dataFactory.races().success(function(data){
        $scope.races = data;
    });
    $scope.toggleMage();
});