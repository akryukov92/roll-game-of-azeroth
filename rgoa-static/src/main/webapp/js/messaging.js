/**
 * Created by Александр on 05.06.2016.
 */
function getWsUri(httpUri, target){
    var new_uri;
    if (httpUri.protocol === "https:") {
        new_uri = "wss:";
    } else {
        new_uri = "ws:";
    }
    new_uri += "//" + httpUri.host + target + "/websocket";
    return new_uri;
}
function first(message){
    if (message.body) {
        alert("first got message with body " + message.body)
    } else {
        alert("first got empty message");
    }
}
function second(message){
    if (message.body) {
        alert("second got message with body " + message.body)
    } else {
        alert("second got empty message");
    }
}
debugger;
var socket = new WebSocket(getWsUri(window.location, "/game/nrp"));
var stompClient = Stomp.over(socket);

var stompClient2 = Stomp.client(getWsUri(window.location, "/game/nrp"));
stompClient.connect({}, function(frame){});
stompClient.subscribe("/game/topic/hello", first, { id: "007"});

stompClient2.connect({}, function(frame){});
stompClient2.subscribe("/game/topic/hello", second, { id: "008"});

stompClient.send("/game/topic/hello", {}, "Hello STOMP");