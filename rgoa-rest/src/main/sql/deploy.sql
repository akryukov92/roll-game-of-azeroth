DROP OWNED BY rgoa_jdbc;
DROP USER IF EXISTS rgoa_jdbc;
CREATE USER rgoa_jdbc PASSWORD '123';

DROP TABLE IF EXISTS attributes;
DROP TABLE IF EXISTS categories;
DROP TABLE IF EXISTS races;
DROP TABLE IF EXISTS limits;
DROP TABLE IF EXISTS draftA;

CREATE TABLE attributes (
    key varchar(3) PRIMARY KEY,
    name varchar,
    type varchar,
    description varchar
);
CREATE TABLE categories (
    category_id uuid PRIMARY KEY,
    name varchar,
    base_sp int,
    base_hp int,
    highest_base_attr int,
    highest_spec_attr int,
    quantity_of_specs int,
    quantity_of_weapons int,
    can_select_category boolean
);
CREATE TABLE races (
    race_id uuid PRIMARY KEY,
    name varchar,
    description varchar,
    bonus_hp integer,
    bonus_ep integer,
    bonus_mp integer
);
CREATE TABLE limits (
    race_id uuid,
    attribute varchar(3),
    lowest integer,
    highest integer,
    PRIMARY KEY (race_id, attribute)
);
CREATE TABLE draftA (
    drafta_id uuid PRIMARY KEY,
    race_id uuid NOT NULL,
    category_id uuid NOT NULL,
    is_mage boolean,
    attribute_values varchar,
    power_source varchar(3),
    spec_attr varchar(3),
    feature_list varchar,
    limits_list varchar
);

GRANT SELECT ON attributes TO rgoa_jdbc;
GRANT SELECT ON limits TO rgoa_jdbc;
GRANT SELECT ON races TO rgoa_jdbc;
GRANT SELECT ON categories TO rgoa_jdbc;
GRANT SELECT, INSERT, DELETE, UPDATE ON draftA TO rgoa_jdbc;

INSERT INTO attributes (key, name, type, description)
VALUES ('str','Сила','physical','Сила');
INSERT INTO attributes (key, name, type, description)
VALUES ('agi','Ловкость','physical','Ловкость');
INSERT INTO attributes (key, name, type, description)
VALUES ('end','Выносливость','physical','Выносливость');
INSERT INTO attributes (key, name, type, description)
VALUES ('int','Интеллект','magical','Аркана, поскольку ей пользуются исключительно через знаия заклятий и манипуляцию энергией.');
INSERT INTO attributes (key, name, type, description)
VALUES ('wil','Сила воли','magical','Философия по типу Света/Тьмы, где знание, хотя тоже важно, отходит на второй план, а важнее всего непоколебимая вера в то, что ты прав/следуешь верному пути.');
INSERT INTO attributes (key, name, type, description)
VALUES ('per','Восприятие','magical','Опора на окружающий мир и внешние силы - духов. Элуну, природу, лоа, Древних Богов - не особенно важно, что - но источник силы четко идентифицирован как разум, не являющийся частью заклинателя. Свет и Тень - Силы безликие и не имеют олицетворений как таковых, если не брать наару, в нашей версии не появлявшихся никак.');

INSERT INTO categories (category_id, name, base_sp, base_hp, highest_base_attr, highest_spec_attr, quantity_of_specs, quantity_of_weapons, can_select_category)
VALUES ('77f252067ec74f27a7fe461d82ca7b9f', 'Обыватель', 28, 6, 2, 2, 0, 1, false);
INSERT INTO categories (category_id, name, base_sp, base_hp, highest_base_attr, highest_spec_attr, quantity_of_specs, quantity_of_weapons, can_select_category)
VALUES ('992f383cfd694cc68000585d2efbcdce', 'Боец', 30, 8, 2, 3, 1, 2, true);
INSERT INTO categories (category_id, name, base_sp, base_hp, highest_base_attr, highest_spec_attr, quantity_of_specs, quantity_of_weapons, can_select_category)
VALUES ('502e100a3f02463284baa451859b381f', 'Воитель', 32, 10, 3, 4, 1, 3, true);
INSERT INTO categories (category_id, name, base_sp, base_hp, highest_base_attr, highest_spec_attr, quantity_of_specs, quantity_of_weapons, can_select_category)
VALUES ('ff63fed8ecb24ce8a5ccdf491449be5b', 'Эксперт', 34, 12, 4, 4, 0, 4, true);

INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('2e6cb5ae657c4c25b5c66ded196eac47', 'Человек', 2, 2, 2);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('43b148cd4597422fb432a42f49d0c812', 'Дварф', 4, 4, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('ddcd6550689d4fe5bb818a7db6a16c9f', 'Ловкий ночной эльф', 2, 4, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('7c51d222bb934fd9b97f3e6c3eb46f7b', 'Умный ночной эльф', 2, 0, 6);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('f16c87f3a8224fc7b93bf2a09ba453dc', 'Гном', 0, 2, 8);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('20209bdb8fde4d848eac0e54850c9450', 'Ловкий высший эльф', 2, 4, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('6eb104dde6284a50bd41bc5de4e62a7e', 'Умный высший эльф', 2, 0, 6);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('3d2ba391aca94af69da92c17c6a10b5c', 'Орк', 4, 4, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('2760c18bd7244042afebe3c36be45f08', 'Ловкий мертвяк', 4, 4, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('0fd9fd0165334b4c8bf95c6639134ea5', 'Умный мертвяк', 4, 0, 4);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('d5c4afba7a664d108dccd97aa9008aeb', 'Ловкий тролль', 4, 4, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('710a1e9036e64eda8afa3d493fe252bf', 'Умный тролль', 4, 0, 4);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('3f24bc43bdfa40f69a090b0e3249c475', 'Таурен', 6, 6, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('c4858d4ce0474279a5c5051aae54ee85', 'Ловкий гоблин', 2, 4, 0);
INSERT INTO races (race_id, name, bonus_hp, bonus_ep, bonus_mp)
VALUES ('bd46982dbb0e4b06aa1fd75cbbbe472c', 'Умный гоблин', 2, 0, 6);

INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('43b148cd4597422fb432a42f49d0c812', 'str', -1, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('43b148cd4597422fb432a42f49d0c812', 'end', 1, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('ddcd6550689d4fe5bb818a7db6a16c9f', 'agi', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('7c51d222bb934fd9b97f3e6c3eb46f7b', 'agi', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('f16c87f3a8224fc7b93bf2a09ba453dc', 'str', -4, 1);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('20209bdb8fde4d848eac0e54850c9450', 'agi', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('6eb104dde6284a50bd41bc5de4e62a7e', 'agi', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('3d2ba391aca94af69da92c17c6a10b5c', 'str', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('3d2ba391aca94af69da92c17c6a10b5c', 'end', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('2760c18bd7244042afebe3c36be45f08', 'end', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('2760c18bd7244042afebe3c36be45f08', 'wil', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('0fd9fd0165334b4c8bf95c6639134ea5', 'end', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('0fd9fd0165334b4c8bf95c6639134ea5', 'wil', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('d5c4afba7a664d108dccd97aa9008aeb', 'str', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('d5c4afba7a664d108dccd97aa9008aeb', 'agi', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('710a1e9036e64eda8afa3d493fe252bf', 'str', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('710a1e9036e64eda8afa3d493fe252bf', 'agi', 0, 5);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('3f24bc43bdfa40f69a090b0e3249c475', 'agi', -4, 3);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('c4858d4ce0474279a5c5051aae54ee85', 'str', -4, 1);
INSERT INTO limits (race_id, attribute, lowest, highest)
VALUES ('bd46982dbb0e4b06aa1fd75cbbbe472c', 'str', -4, 1);