import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.util.Assert;
import ru.akryukov.dto.CharacterDraftA;

import java.io.IOException;

/**
 * @author akryukov
 *         09.08.2016
 */
public class DeserializerUnitTest {
    @Test
    public void test() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String source = "{\"raceId\":\"7c51d222-bb93-4fd9-b97f-3e6c3eb46f7b\",\"categoryId\":\"992f383c-fd69-4cc6-8000-585d2efbcdce\",\"isMage\":false,\"powerSource\":\"nil\",\"specAttr\":\"str\",\"attributes\":{\"str\":3,\"agi\":0,\"end\":1,\"int\":1,\"wil\":1,\"per\":-1}}";
        CharacterDraftA draftA;
        draftA = mapper.readValue(source, CharacterDraftA.class);
        Assert.notNull(draftA);
    }
}
