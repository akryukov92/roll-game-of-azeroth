package ru.akryukov.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * @author akryukov
 *         15.07.2016
 */
public class Race {
    private UUID id;
    private int bonusHP;
    private int bonusEP;
    private int bonusMP;
    private String name;

    private List<AttributeLimits> limits;

    public Race(UUID id, int bonusHP, int bonusEP, int bonusMP, String title, List<AttributeLimits> limits) {
        this.id = id;
        this.bonusHP = bonusHP;
        this.bonusEP = bonusEP;
        this.bonusMP = bonusMP;
        this.name = title;
        this.limits = limits;
    }

    public int getBonusHP() {
        return bonusHP;
    }

    public int getBonusEP() {
        return bonusEP;
    }

    public int getBonusMP() {
        return bonusMP;
    }

    public String getName() {
        return name;
    }

    public List<AttributeLimits> getLimits() {
        return Collections.unmodifiableList(limits);
    }

    public UUID getId() {
        return id;
    }

    public static Builder builder(){
        return new Builder();
    }

    public static class Builder {
        private UUID id;
        private int bonusHP;
        private int bonusEP;
        private int bonusMP;
        private String name;
        private List<AttributeLimits> limits;

        private Builder(){
            limits = new ArrayList<>();
        }

        public Builder withId(Object value){
            this.id = UUID.fromString(value.toString());
            return this;
        }

        public Builder withBonusHP(Object value) {
            this.bonusHP = Integer.parseInt(value.toString());
            return this;
        }
        public Builder withBonusEP(Object value) {
            this.bonusEP = Integer.parseInt(value.toString());
            return this;
        }
        public Builder withBonusMP(Object value) {
            this.bonusMP = Integer.parseInt(value.toString());
            return this;
        }
        public Builder withName(Object value) {
            this.name = value.toString();
            return this;
        }
        public Builder addLimit(AttributeLimits value){
            this.limits.add(value);
            return this;
        }

        public Race build(){
            return new Race(id, bonusHP, bonusEP, bonusMP, name, limits);
        }
    }
}
