package ru.akryukov.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author akryukov
 *         10.08.2016
 */
public class Attribute {
    private String key;
    private String name;
    private String type;
    private String desc;

    public Attribute(
        String key,
        @JsonProperty("name") String name,
        @JsonProperty("type") String type,
        @JsonProperty("desc") String desc
    ) {
        this.key = key;
        this.name = name;
        this.type = type;
        this.desc = desc;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
