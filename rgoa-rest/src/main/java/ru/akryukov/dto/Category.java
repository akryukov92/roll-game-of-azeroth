package ru.akryukov.dto;

/**
 * @author akryukov
 *         15.07.2016
 */
public class Category {
    private String id;
    private String name;
    private int baseSP;
    private int baseHP;
    private int highestBaseAttribute;
    private int highestSpecAttribute;
    private int quantityOfSpecAttributes;
    private int quantityOfWeapons;
    private boolean canSelectCategory;

    public Category(String id, String name, int baseSP, int baseHP, int highestBaseAttribute, int highestSpecAttribute, int quantityOfSpecAttributes, int quantityOfWeapons, boolean canSelectCategory) {
        this.id = id;
        this.name = name;
        this.baseSP = baseSP;
        this.baseHP = baseHP;
        this.highestBaseAttribute = highestBaseAttribute;
        this.highestSpecAttribute = highestSpecAttribute;
        this.quantityOfSpecAttributes = quantityOfSpecAttributes;
        this.quantityOfWeapons = quantityOfWeapons;
        this.canSelectCategory = canSelectCategory;
    }

    public String getName() {
        return name;
    }

    public int getBaseSP() {
        return baseSP;
    }

    public int getBaseHP() {
        return baseHP;
    }

    public int getHighestBaseAttribute() {
        return highestBaseAttribute;
    }

    public int getHighestSpecAttribute() {
        return highestSpecAttribute;
    }

    public int getQuantityOfSpecAttributes() {
        return quantityOfSpecAttributes;
    }

    public int getQuantityOfWeapons() {
        return quantityOfWeapons;
    }

    public String getId() {
        return id;
    }

    public boolean isCanSelectCategory() {
        return canSelectCategory;
    }
}
