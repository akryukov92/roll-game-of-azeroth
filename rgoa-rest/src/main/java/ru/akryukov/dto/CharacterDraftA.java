package ru.akryukov.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * @author akryukov
 *         08.08.2016
 */
public class CharacterDraftA {
    private String draftId;
    private String raceId;
    private String categoryId;
    private Boolean isMage;
    private Map<String,Integer> attributes;
    private String powerSource;
    private String specAttr;
    private Map<String, Integer> features;
    private Map<String, Integer> limitations;

    public String getDraftId() {
        return draftId;
    }

    public String getRaceId() {
        return raceId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public Boolean isMage() {
        return isMage;
    }

    public Map<String, Integer> getAttributes() {
        return attributes;
    }

    public String getPowerSource() {
        return powerSource;
    }

    public String getSpecAttr() {
        return specAttr;
    }

    public Map<String, Integer> getFeatures() {
        return features;
    }

    public Map<String, Integer> getLimitations() {
        return limitations;
    }

    public CharacterDraftA(){}

    public CharacterDraftA(
        @JsonProperty("draftId") String draftId,
        @JsonProperty("raceId") String raceId,
        @JsonProperty("categoryId") String categoryId,
        @JsonProperty("mage") Boolean isMage,
        @JsonProperty("attributes") Map<String, Integer> attributes,
        @JsonProperty("powerSource") String powerSource,
        @JsonProperty("specAttr") String specAttr,
        @JsonProperty("features") Map<String, Integer> features,
        @JsonProperty("limitations") Map<String, Integer> limitations
    ){
        this.draftId = draftId;
        this.raceId = raceId;
        this.categoryId = categoryId;
        this.isMage = isMage;
        this.attributes = attributes;
        this.powerSource = powerSource;
        this.specAttr = specAttr;
        this.features = features;
        this.limitations = limitations;
    }

    public CharacterDraftA(String draftId, CharacterDraftA draftA){
        this.draftId = draftId;
        this.raceId = draftA.raceId;
        this.categoryId = draftA.categoryId;
        this.isMage = draftA.isMage;
        this.attributes = new HashMap<>();
        this.attributes.putAll(draftA.attributes);
        this.powerSource = draftA.powerSource;
        this.specAttr = draftA.specAttr;
        this.features = new HashMap<>();
        this.features.putAll(draftA.features);
        this.limitations = new HashMap<>();
        this.limitations.putAll(draftA.limitations);
    }
}
