package ru.akryukov.dto;

public class Specialization {
    private String name;
    private String description;
    private String powerAttr;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPowerAttr() {
        return powerAttr;
    }

    public Specialization(String name, String description, String powerAttr) {
        this.name = name;
        this.description = description;
        this.powerAttr = powerAttr;
    }
}
