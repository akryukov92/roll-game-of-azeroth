package ru.akryukov.dto;

/**
 * @author akryukov
 *         15.07.2016
 */
public class AttributeLimits {
    private String race_id;
    private String attributeName;
    private int min;
    private int max;

    public AttributeLimits(String race_id, String attribute_name, int min, int max) {
        this.race_id = race_id;
        this.attributeName = attribute_name;
        this.min = min;
        this.max = max;
    }

    public String getRace_id() {
        return race_id;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
