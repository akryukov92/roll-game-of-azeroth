package ru.akryukov.Utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author akryukov
 *         15.08.2016
 */
public class StringUtils {
    private static ObjectMapper _mapper;

    public static void setObjectMapper(ObjectMapper mapper){
        _mapper = mapper;
    }
    public static Map<String, Integer> toMap(String source){
        try {
            return _mapper.readValue(source, Map.class);
        } catch (IOException e) {
            return new HashMap<>();
        }
    }
    public static String toString(Map<String, Integer> source){
        try {
            return _mapper.writeValueAsString(source);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public static String join(CharSequence delimiter, List<String> elements){
        if (delimiter == null){
            throw new IllegalArgumentException("Delimiter should not be null");
        }
        if (elements == null){
            throw new IllegalArgumentException("Elements list should not be empty");
        }
        StringBuilder builder = new StringBuilder();
        CharSequence d = "";
        for (CharSequence item : elements){
            builder.append(d);
            builder.append(item);
            d = delimiter;
        }
        return builder.toString();
    }

    public static List<String> split(String delimiter, String elements){
        List<String> ret = new ArrayList<>();
        if (elements == null){
            return ret;
        }
        Collections.addAll(ret, elements.split(delimiter));
        return ret;
    }

    public static List<String> toList(String[] array){
        if (array == null){
            throw new IllegalArgumentException("Elements list should not be empty");
        }
        List<String> ret = new ArrayList<>();
        Collections.addAll(ret, array);
        return ret;
    }
}
