package ru.akryukov.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.akryukov.dao.AttributesDao;
import ru.akryukov.dao.CategoriesDao;
import ru.akryukov.dao.CharacterDraftADao;
import ru.akryukov.dao.RacesDao;
import ru.akryukov.dto.Attribute;
import ru.akryukov.dto.Category;
import ru.akryukov.dto.CharacterDraftA;
import ru.akryukov.dto.Race;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author akryukov
 *         04.08.2016
 */
@RestController
@RequestMapping("/creation")
public class CreationController {
    private static final Logger logger = Logger.getLogger(CreationController.class);

    private CategoriesDao categoriesDao;
    private CharacterDraftADao draftADao;
    private RacesDao racesDao;
    private AttributesDao attributesDao;

    @Autowired
    public CreationController(
        CategoriesDao categoriesDao,
        RacesDao racesDao,
        CharacterDraftADao draftADao,
        AttributesDao attributesDao
    ){
        this.categoriesDao = categoriesDao;
        this.racesDao = racesDao;
        this.draftADao = draftADao;
        this.attributesDao = attributesDao;
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = "/categories"
    )
    @ResponseBody
    public List<Category> getCategories(){
        return categoriesDao.getAll();
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = "/races"
    )
    @ResponseBody
    public List<Race> getRaces() {
        return racesDao.getAll();
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = "/attributes"
    )
    @ResponseBody
    public Map<String, Attribute> getAttributes(){
        return attributesDao.getAll();
    }

    @RequestMapping(
        method = RequestMethod.POST,
        value = "/submitDraftA"
    )
    @ResponseBody
    public CharacterDraftA submitRaceAndAttributes(@RequestBody CharacterDraftA draftA)
        throws JsonProcessingException
    {
        if (draftA.getDraftId() == null || draftA.getDraftId().isEmpty()) {
            return new CharacterDraftA(draftADao.storeDraftA(draftA), draftA);
        } else {
            draftADao.updateDraftA(draftA);
            return draftA;
        }
    }

    @RequestMapping(
        method = RequestMethod.GET,
        value = "/getDraftAById"
    )
    @ResponseBody
    public CharacterDraftA getById(@RequestParam(value = "id") String draftAId) throws IOException {
        UUID id = UUID.fromString(draftAId);
        return draftADao.getById(id);
    }
}
