package ru.akryukov.controllers;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author akryukov
 *         28.07.2016
 */
@RestController
public class GreetingsController {
    private static final Logger logger = Logger.getLogger(GreetingsController.class);

    @RequestMapping(
        method = RequestMethod.GET,
        value = "/diagnostics"
    )
    @ResponseBody
    public String handleDiagnostics(){
        logger.info("request /diagnostics");
        return "It works";
    }
}