package ru.akryukov.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import ru.akryukov.Utils.StringUtils;
import ru.akryukov.dto.CharacterDraftA;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * @author akryukov
 *         09.08.2016
 */
@Repository
public class CharacterDraftADao {
    private static Logger logger = Logger.getLogger(CharacterDraftADao.class);

    private JdbcTemplate jdbcTemplate;

    private static String INSERT_DRAFT_A_QUERY =
        "INSERT INTO draftA(drafta_id, race_id, category_id, is_mage, attribute_values, power_source, spec_attr, feature_list, limits_list)" +
        " VALUES (?,?,?,?,?,?,?,?,?);";

    private static String UPDATE_DRAFT_A_QUERY =
        "UPDATE draftA SET" +
            " race_id = ?" +
            ", category_id = ?" +
            ", is_mage = ?" +
            ", attribute_values = ?" +
            ", power_source = ?" +
            ", spec_attr = ?" +
            ", feature_list = ?" +
            ", limits_list = ?" +
        "WHERE drafta_id = ?";

    private static String SELECT_DRAFT_A_QUERY =
        "SELECT" +
            " drafta_id" +
            ", race_id" +
            ", category_id" +
            ", is_mage" +
            ", attribute_values" +
            ", power_source" +
            ", spec_attr" +
            ", COALESCE(feature_list,'') feature_list" +
            ", COALESCE(limits_list,'') limits_list" +
            " FROM draftA" +
            " WHERE drafta_id = ?";

    @Autowired
    public CharacterDraftADao(DataSource dataSource){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public String storeDraftA(CharacterDraftA draftA) throws JsonProcessingException {
        UUID uuid = UUID.randomUUID();
        jdbcTemplate.update(INSERT_DRAFT_A_QUERY,
            uuid,
            UUID.fromString(draftA.getRaceId()),
            UUID.fromString(draftA.getCategoryId()),
            draftA.isMage(),
            StringUtils.toString(draftA.getAttributes()),
            draftA.getPowerSource(),
            draftA.getSpecAttr(),
            StringUtils.toString(draftA.getFeatures()),
            StringUtils.toString(draftA.getLimitations())
        );
        return uuid.toString();
    }

    public void updateDraftA(CharacterDraftA draftA) throws JsonProcessingException {
        jdbcTemplate.update(UPDATE_DRAFT_A_QUERY,
            UUID.fromString(draftA.getRaceId()),
            UUID.fromString(draftA.getCategoryId()),
            draftA.isMage(),
            StringUtils.toString(draftA.getAttributes()),
            draftA.getPowerSource(),
            draftA.getSpecAttr(),
            StringUtils.toString(draftA.getFeatures()),
            StringUtils.toString(draftA.getLimitations()),
            UUID.fromString(draftA.getDraftId())
        );
    }

    public CharacterDraftA getById(UUID id) throws IOException {
        return jdbcTemplate.query(SELECT_DRAFT_A_QUERY, new Object[]{id}, new ResultSetExtractor<CharacterDraftA>() {
            @Override
            public CharacterDraftA extractData(ResultSet rs) throws SQLException, DataAccessException {
                if(rs.next()){
                    return new CharacterDraftA(
                        rs.getString("drafta_id"),
                        rs.getString("race_id"),
                        rs.getString("category_id"),
                        rs.getBoolean("is_mage"),
                        StringUtils.toMap(rs.getString("attribute_values")),
                        rs.getString("power_source"),
                        rs.getString("spec_attr"),
                        StringUtils.toMap(rs.getString("feature_list")),
                        StringUtils.toMap(rs.getString("limits_list"))
                    );
                }
                return null;
            }}
        );
    }
}
