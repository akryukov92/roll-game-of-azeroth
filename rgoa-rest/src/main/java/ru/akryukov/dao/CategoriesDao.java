package ru.akryukov.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.akryukov.dto.Category;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author akryukov
 *         18.07.2016
 */
@Repository
public class CategoriesDao {
    private static Logger logger = Logger.getLogger(CategoriesDao.class);

    private JdbcTemplate jdbcTemplate;

    private static String SELECT_CATEGORIES_QUERY =
        "SELECT category_id," +
            " name," +
            " base_sp," +
            " base_hp, " +
            " highest_base_attr," +
            " highest_spec_attr," +
            " quantity_of_specs," +
            " quantity_of_weapons," +
            " can_select_category " +
        " FROM categories;";

    @Autowired
    public CategoriesDao(DataSource dataSource){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Category> getAll(){
        List<Map<String, Object>> categoriesDataList = jdbcTemplate.queryForList(SELECT_CATEGORIES_QUERY);
        List<Category> categories = new ArrayList<>();
        for (Map<String, Object> item : categoriesDataList) {
            categories.add(new Category(
                item.get("category_id").toString(),
                item.get("name").toString(),
                Integer.parseInt(item.get("base_sp").toString()),
                Integer.parseInt(item.get("base_hp").toString()),
                Integer.parseInt(item.get("highest_base_attr").toString()),
                Integer.parseInt(item.get("highest_spec_attr").toString()),
                Integer.parseInt(item.get("quantity_of_specs").toString()),
                Integer.parseInt(item.get("quantity_of_weapons").toString()),
                Boolean.parseBoolean(item.get("can_select_category").toString())));
        }
        return categories;
    }
}
