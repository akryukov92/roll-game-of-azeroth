package ru.akryukov.dao;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.akryukov.dto.AttributeLimits;
import ru.akryukov.dto.Race;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author akryukov
 *         15.07.2016
 */
@Repository
public class RacesDao {
    private static Logger logger = Logger.getLogger(RacesDao.class);

    private JdbcTemplate jdbcTemplate;
    private static String SELECT_RACES_QUERY =
        "SELECT race_id" +
        ", name" +
        ", bonus_hp" +
        ", bonus_ep" +
        ", bonus_mp" +
        " FROM races;";

    private static String SELECT_LIMITS_QUERY =
        "SELECT race_id" +
        ", attribute" +
        ", lowest" +
        ", highest" +
        " FROM limits;";

    @Autowired
    public RacesDao(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<Race> getAll(){
        logger.debug("getAll called");
        List<Map<String, Object>> racesDataList = jdbcTemplate.queryForList(SELECT_RACES_QUERY);
        Map<Object,Race.Builder> raceBuilders = new HashMap<>();
        for (Map<String,Object> item: racesDataList) {
            Race.Builder builder = Race.builder();
            builder.withId(item.get("race_id"));
            builder.withBonusEP(item.get("bonus_ep"));
            builder.withBonusHP(item.get("bonus_hp"));
            builder.withBonusMP(item.get("bonus_mp"));
            builder.withName(item.get("name"));
            raceBuilders.put(item.get("race_id"), builder);
        }
        List<Map<String, Object>> limitsDataList = jdbcTemplate.queryForList(SELECT_LIMITS_QUERY);
        for (Map<String, Object> item : limitsDataList)  {
            if (raceBuilders.containsKey(item.get("race_id"))){
                raceBuilders.get(item.get("race_id")).addLimit(new AttributeLimits(
                    item.get("race_id").toString(),
                    item.get("attribute").toString(),
                    Integer.parseInt(item.get("lowest").toString()),
                    Integer.parseInt(item.get("highest").toString())
                ));
            }
        }
        List<Race> races = new ArrayList<>();
        for (Map.Entry<Object, Race.Builder> builder: raceBuilders.entrySet()){
            races.add(builder.getValue().build());
        }
        return races;
    }
}
