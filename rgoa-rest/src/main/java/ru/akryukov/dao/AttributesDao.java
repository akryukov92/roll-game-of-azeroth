package ru.akryukov.dao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.akryukov.dto.Attribute;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author akryukov
 *         10.08.2016
 */
@Repository
public class AttributesDao {
    private static Logger logger = Logger.getLogger(CategoriesDao.class);

    private JdbcTemplate jdbcTemplate;

    private static String SELECT_ATTRIBUTES_QUERY =
        "SELECT key, name, type, description FROM attributes";

    @Autowired
    public AttributesDao(DataSource dataSource){
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Map<String, Attribute> getAll(){
        List<Map<String, Object>> attributesDataList = jdbcTemplate.queryForList(SELECT_ATTRIBUTES_QUERY);
        Map<String, Attribute> attributeMap = new HashMap<>();
        for (Map<String, Object> item : attributesDataList){
            Attribute temp = new Attribute(
                item.get("key").toString(),
                item.get("name").toString(),
                item.get("type").toString(),
                item.get("description").toString());
            attributeMap.put(temp.getKey(), temp);
        }
        return attributeMap;
    }
}
