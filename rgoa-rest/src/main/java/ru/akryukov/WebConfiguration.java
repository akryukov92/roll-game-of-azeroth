package ru.akryukov;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by AKryukov on 14.07.2015.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"ru.akryukov"})
public class WebConfiguration extends WebMvcConfigurerAdapter {
}
